# Installation de logiciels et mises à jour de la distribution

## Installation à partir des sources

On trouve publié sur l'Internet des source de logiciels sous forme
d'archive .tar.* ou encore de dépôts git. Si le logiciel dans la
version qui vous convient n'est pas dans un dépôt Debian on peut
vouloir l'install "à la main" (important : il faut s'assurer qu'il
va dans /usr/local (parfois /opt)

Comment on lance la compilation : installer les compilateurs nécessaires
(paquet build-essential : C/C++ et Make), configurer et lancer la
compilation et lancer l'installation (en root)

Cas typique : si le projet logiciel utilise "autoconf" :

~~~~
$ tar xf software.tar.gz
$ cd software
$ vi README / vi INSTALL
$ ./configure ... option : --prefix=/usr/local (defaut)
$ make
$ sudo make install
~~~~

Parfois on a directement un Makefile.

Example : on va faire comme si le logiciel xosview n'était pas dans les dépôts.


Le source se trouve dans un dépôt GIT : https://github.com/hills/xosview

On commence par cloner se dépot et on regarde ce qu'il contient.

~~~~
$ git clone https://github.com/hills/xosview
$ cd xosview
$ ls
$ vi README
$ make
... échoue : il manque des dépendance de construction.
~~~~

## On va résoudre les dépendance de construction

Outils utiles pour déterminer ces dépendances ?

- apt-cache ou apt avec le mot clef search

- apt-file : recherche dans les contenus des paquets disponibles

Pour installer et initialiser apt-file

~~~~
$ sudo apt install apt-file
$ sudo apt-file update
$ apt-file search fichier_manquant
~~~~

## Installation à partir de paquets Debian

Sources sont des sites Web public ou des mirroirs locaux

- Dépôts Debian officiels
- Dépôts tiers (attention à la confiance !) : PostgreSQL, Docker, etc.

Les paquets .deb

- fichiers du logiciel ou du composant concerné
- informations de dépendances
- descriptions du produit
- script de pre & post installation et désinstallation

Outils :

- `dpkg` : manipuler un .deb individuel et interroger la base de donnée
 des paquets installés :
- apt-get, apt-cache, apt, apt-file : gère les dépendances et télécharge
et install à partir des dépôts

La configuration de apt : /etc/apt/apt.conf et /etc/apt/apt.conf.d

Les dépôts sont dans /etc/apt/sources.list (laissez uniquement les dépôt
officiels là) et /etc/apt/sources.list.d/autredepot.list

Les paquets sont signés. Si vous ajoutez un dépot vous devez enregistrer
une nouvelle clef publique de la source (de confiance).

~~~~
$ dpkg -l
$ dpkg -i software-version.deb
$ apt -f install # régler les pb de dépendance
$ dpkg-reconfigure paquet
$ dpkg -r paquet # supprime mais garde la conf
$ dpkg --purge paquet # supprime complètement
$ apt remove paquet # gére les dépendance
$ dpkg -S /bin/ls # origine d'un fichier
$ apt update # màj la liste des paquets disponibles
$ apt upgrade # màj pour sans changer de version de distri
$ apt full-upgrade # idem mais enlève les paquets devenus inutiles
$ apt dist-upgrade # après changement de version de distri dans les sources
~~~~

TP1. Activer le dépot Debian tiers de PostgreSQL : https://www.postgresql.org/

Deux distributions (ou compléments de distribution) intéressantes NIX et GUIX.

