# Noyau et modules

L'image noyau de démarrage : `/boot/vmlinux-$( uname -r )`

Les modules chargeables : `/lib/modules/$(uname -r)/`

Commandes utiles :

~~~~Bash 
$ sudo dmesg
$ lsmod # liste des modules chargées
$ modinfo e1000 # infos sur un module
$ lsusb
$ lspci
~~~~

Beaucoup d'information dans les systèmes de fichiers synthétiques 
montés dans /proc et /sys

Dev noyau : 

http://www.kernel.org/

http://www.bootlin.com : société spécialisée dans l'embarqué Linux et
le temps réel

## Paramétrage du noyau

Dans `/proc/sys` fichiers qui indiquent des paramètres noyau courant et
qui acceptent l'écriture.

ex : 

~~~~
# cat /proc/sys/net/ipv4/ip_forward # sommes nous un routeur ?
0
# echo 1 > /proc/sys/net/ipv4/ip_forward # maintenant oui !
# sysctl -w net.ipv4.ip_forward=1 # idem ci-dessus
~~~~

Pour rendre définitif ce réglage : /etc/systcl.conf qui inclut les
fichiers présent dans `/etc/sysctl.d/*.conf`
