# Démarrage du système

Sur PC (32 puis 64 bits) : historiquement à partir du 386 on a eu un
démarrage "historique" : BIOS (compatible MSDOS) avec beaucoup de
limitations :

BIOS (ROM) charge 446 octets présents dans le Master Boot Record qui
est le premier secteur du disque dur "de démarrage" : on y trouve
l'amorce d'un bootloader (LILO puis GRUB 1 puis 2) qui va chercher
secteur par secteur le bootloader secondaire qui propose un menu
permettant de charger et démarrer un ou plusieurs noyaux Linux.

Le nouveau firmware des PC : UEFI, un firmware moderne avec une
ligne de commande (proche de dos et Unix) et un pilote de système
de fichier FAT (~ MS-DOS), il faut une partition spécifique de
ce type sur le disque. On le voit en faisant `lsblk` il y a 
bien une partition sda1 montée sur /boot/efi.

Pour agir sur la configuration du démarrage on va agir sur
`/etc/default/grub` et lancer `grub-mkconfig > /boot/grub/grub.cfg`
qui va combiner la configuration avec les modèle présents dans
`/etc/grub.d`.

ex: augmenter le délai pendant lequel le menu grub est visible.
(modifier /etc/default/grub et faire le grub-mkconfig)

## TP : Reprendre le contrôle d'un système dont on a oublié le mot de passe root

(sur le second Debian)

Le redémarrer (éventuellement sauvagement à partir de Virtual Box)

Note : tout en QWERTY !!!

- Au menu de GRUB : e (edit)

- on va modifier la ligne qui charge le noyau Linux : "... vmlinuz..." :
on enlève "quiet" et surtout on ajoute "init=/bin/bash"
(ça indique au noyau qu'au lieu de lancer /sbin/init il va lancer simplement
un Shell)

- on va obtenir un Shell et la racine est montée en lecture seule on 
va la remonter en lecture/écriture : `# mount -o rw,remount /`

- on réparre le système : `# passwd root` (attention : QWERTY) mettez
un mdp simple (toto) quitte à le changer après...

- on remonte la racine en lecture seule `# mount -o ro,remount` 

- on redémarre sauvagement (équivalent de power off/on)

## Les étapes principales du démarrage


BIOS/UEFI -> Bootload GRUB2 -> charge le Noyau Linux + image du ram disk initial -> le noyau peut monte ce ram disk initial -> monter / -> exécuter /sbin/init

Examinez grub.cfg : les lignes "linux ... vmlinuz-...." chargent le noyau et les lignes
"initrd ..." chargent le ram disk initial.

## TP2 : extraire le contenu de notre ramdisk initial dans /tmp/initrd :

~~~~Bash
jpierre@morlaix:~$ uname -r
5.10.0-26-amd64
jpierre@morlaix:~$ ls -l /boot/vmlinuz-5.10.0-26-amd64 
-rw-r--r-- 1 root root 7044672 29 sept. 06:25 /boot/vmlinuz-5.10.0-26-amd64
jpierre@morlaix:~$ ls -l /boot/initrd.img-5.10.0-26-amd64 
-rw-r--r-- 1 root root 43716170  9 oct.  10:57 /boot/initrd.img-5.10.0-26-amd64
jpierre@morlaix:~$ cp /boot/initrd.img-5.10.0-26-amd64 /tmp
jpierre@morlaix:~$ cd /tmp
jpierre@morlaix:/tmp$ file initrd.img-5.10.0-26-amd64 
initrd.img-5.10.0-26-amd64: gzip compressed data, was "mkinitramfs-MAIN_F42LOv", last modified: Mon Oct  9 08:57:21 2023, from Unix, original size modulo 2^32 155258880
jpierre@morlaix:/tmp$ mv initrd.img-5.10.0-26-amd64 initrd.gz
jpierre@morlaix:/tmp$ gunzip initrd.gz 
jpierre@morlaix:/tmp$ file initrd 
initrd: ASCII cpio archive (SVR4 with no CRC)
jpierre@morlaix:/tmp$ mkdir initrd-content
jpierre@morlaix:/tmp$ cd initrd-content/
jpierre@morlaix:/tmp/initrd-content$ cpio -i < ../initrd
303240 blocs
jpierre@morlaix:/tmp/initrd-content$ ls
bin  conf  etc  init  lib  lib32  lib64  libx32  run  sbin  scripts  usr  var
jpierre@morlaix:/tmp/initrd-content$ du -sh .
152M	.
$ ls
bin  conf  etc  init  lib  lib32  lib64  libx32  run  sbin  scripts  usr  var
$ less init
~~~~

Comment le noyau sait quelle le système de fichier racine ? Il reçoit un argument
par le GRUB : root=...

- Le périphérique : /dev/sda2 ; pas nécessairement stable

- Le label du système de fichier : "/" typiquement ; pb si un nouveau disque est
 ajouté qui contient un système de fichier avec le(s) même(s) label(s)
 (solution : écraser le début du disque avec dd : `dd if=/dev/zero of=/dev/sdbX count=10 bs=1M`)

- `UUID=id_aléatoire_calculé_au_formatage` ; problème avec les clonage de volume sur les hyperviseurs
  (Cloud, Proxmox, etc.)

On retrouvera la même problématique potentielle quand on évoquera le stockage.

Une fois la racine montée en lecture seule le noyau exécute `/sbin/init` ouf on entre
enfin dans le _userland_.

