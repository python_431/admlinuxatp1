# La mise en place de l'espace utilisateur

UNIX a connu deux branches principale : BSD et System V (cf. man ps),
chacune avec son init spécifique.

Les distributions GNU/Linux ont du faire des choix.

Marginalement quelques unes ont choisi un boot à la BSD (Slackware)

Debian et Red Hat ont choisi d'abord un boot à la System V (une
conf dans /etc/inittab, basé sur des niveaux d'exécution (0 : arrêt,
1/S : mono-utilisateur, 2-3-4-5 : "normal", 6 : reboot) et les
différentes actions (monter des volumes, configurer le réseau, lancer
des services) étaient définies dans /etc/init.d/nom avec des
liens symboliques placés dans /etc/rcN.d/[SK]NNnom vers ces scripts.

Il y a plus de dix certains ont voulu réformer tout ça.

Une remplacement d'init a été proposé en premier : upstart ; il a
été choisi par Red Hat et Ubuntu mais pas Debian, initiallement il
a été configuré pour imiter init system V.

Red Hat a lancé le développement d'un remplacement : systemd

Tout le monde (ou presque) est passé à systemd !

# L'organisation de systemd

La notion d'unité (actions ou des services) définies par des fichiers
descriptifs (pas des scripts) dans `/lib/systemd/system/` 

- _target_ : états intermédiaire finaux à atteindre :
  
   * local-fs
   * multiuser.target
   * graphical.target

ex : `sudo systemctl set-default multi-user.target` sur votre Debian 1
     puis `sudo reboot` : plus d'invite graphique de connexion

pour rétablir : `sudo systemctl set-default graphical.target`

Le contrôle de systemd se fait avec une commande `systemctl` (et d'autre commandes
dont le nom se termine en "ctl")

ex: `hostnamectl set-hostname morlaix2` (par exemple)

ex de commandes :

~~~~
$ systemctl --show-types list-units  
$ systemctl --show-types list-dependencies
~~~~

Le point on agit le plus souvent ce sont les services (réseau ou autres) applicatifs.

ex: Le serveur web Apache : le paquet s'appelle apache2 et tout le reste (conf, service) aussi.

~~~~
$ apt search apache
$ apt show apache2
$ sudo apt install apache2
$ systemctl status apache2

-> actif (running) : il tourne

-> enabled : automatiquement lancé au démarrage

~~~~
$ sudo systemctl stop apache2 # arrête Apache2 HTTP Server
$ sudo systemctl disable apache2 # le désactive pour les prochains démarrages
$ sudo systemctl start apache2 # le lance
$ sudo systemctl enable apache2 # l'active pour les prochains démarrages
~~~~

Il peut avoir d'autres actions : reload par exemple.

Le service est défini dans `/lib/systemd/system/apache2.service` 

Ex : introduisont une erreur dans la conf d'Apache (remplacer Root par Rooot dans `/etc/apache2/sites-available/*default`)

~~~~
$ sudo systemctl restart apache2
$ sudo systemctl status apache2
$ sudo journalctl -xe
$ sudo journalctl -u apache2
~~~~

On voit quelle ligne de quel fichier de conf pose problème.

## Les évènements systèmes et Linux : où en est-on ?

Historiquement pour les fichiers journaux on avait deux logiques :

- Certains services écrivent directement sans /var/log (ex : Apache2)
- Les messages sont transmis à une service de type "syslog" (démon rsyslog)

   un peu structuré : nom d'hôte, date, catégorie, gravité, texte

  en suivant la configuration dans /etc/rsyslog.conf qui inclue les
  fichiers ajouté par l'admin : /etc/rsyslog.d/*.conf

  -> indique selon la catégorie et la gravité dans quel(s) fichier(s) de
  /var/log/ stocker ou pas les messages

Depuis systemd on a un nouveau processus : journald qui reçoit tous les
messages des unités (en particulier les services)

journald stocke tous ces messages dans /var/log/journal sous forme binaire
très structuré (nom de l'unité, l'utilisateur concerné, le n-ième boot
dans le passé).

Les messages se retrouvent tous à la fois dans le journal binaire (qu'on
peut interroger avec journalctl) et, probablement, dans un des fichiers
de /var/log : journald et rsyslog se passent respectivement leurs messages.

Ce que permet journald/journalctl c'est d'interroger les logs comme une
vraie base de données structurée :

~~~~
$ sudo journalctl --since "2023-10-11" --until "2023-10-12 15:00"
$ sudo journalctl -p 3
~~~~

Bonne idée (pour éviter sudo) : `sudo usermod -a -G adm,systemd-journal user_admin_id`

cf. https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs-fr

## Outils d'analyse d'activité des processus

Ils restent pertinents pour débuguer de plus près :

~~~~
$ ps aux
$ ps -elf
$ top
$ htop
$ sudo iotop
$ sudo vmstat
$ sudo iostat
$ sudo sar
...
~~~~
