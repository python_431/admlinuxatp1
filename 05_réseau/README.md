# Configuration du réseau et le partage de ressources

Traditionellement la configuration était (et peut toujours être) dans
des fichiers plats : /etc/network/interfaces /etc/network/interfaces.d/...

Un nouveau système de configuration du réseau est apparu : Network Manager
qui stocke sa configuration dans /var/lib (à ne pas toucher) et qui se
configure avec trois types d'outils :

- nmcli (ligne de commande)
- nmtui (pseudo-graphique)
- bureau graphique 

Le DNS est historiquement défini dans /etc/resolv.conf

Network Manager prend en compte aussi les fichiers plats historiques.

La syntaxe des fichiers plats :

DHCP :

~~~~
auto enp0s3
iface inet dhcp
~~~~

En statique :

~~~~
auto enp0s3
iface enp0s3 inet static
   address 10.1.42.1/24
   gateway 10.1.42.255
~~~~

avec nmcli :

~~~~
$ sudo nmcli con add/mod enp0s3 ipv4.address 10.1.42.1/24
$ sudo nmcli con mod enp0s3 ipv4.gateway 10.1.42.255
$ sudo nmcli con mod enp0s3 ipv4.dns ip_du_dns
~~~~ 

La commande de bas niveau : `ip` :

~~~~
$ ip addr show
$ ip link show
$ ip route show
$ ip addr add dev enp0s3 10.1.42.1/24 # pas permanent !
~~~~

Network Manager lit 1) les fichier plats 2) sa base de données et exécute nmcli qui exécute ip.

## Services réseaux et partage de ressources

Quand on installe un service réseau (avec apt) normalement il se lance automatiquement
(ex Apache2, nginx, etc. sauf le serveur DHCP) 

Pour vérifier : l'outil `ss` : ex `ss -listen` (ou netstat, lsof -i)

## Partage de fichiers

NFS : Network File System

v3 et v4 : très différentes au niveau protocole, préférer v4.

Installer le serveur NFS : `sudo apt install nfs-kernel-server`

La configuration : /etc/exports

En NFSv4 tous les répertoires exportés doivent avoir une base commune (qui n'est pas / !)

~~~~
/srv  *(ro, no_subtree_check)
/srv/ill 192.168.1.0/24(rw, no_subtree_check)
/srv/atp 192.168.1.0/24(rw, no_subtree_check)
~~~~

On relance le serveur NFS : `systemctl restart nfs 
