# Configuration de Samba

Samba fournit une implémentation des protocoles de partage de
ressources propres à Microsoft Windows sous UNIX.

Installation et positionnement des mots de passes (pour
l'accès via smb seulement) des utilisateurs concernés :

~~~~Bash
$ sudo apt install samba smbclient
$ sudo smbpasswd -a jpierre
$ sudo smbpasswd -a joe
$ sudo smbpasswd -a jane
~~~~

On peut ajouter des partages dans `/etc/samba/smb.conf` (par
défaut seul les dossiers personnels sont exportés par Samba.

~~~~
[atp]
  comment = Des données...
  path = /srv/atp
  browseable = yes
  read only = no
  guest ok = yes

[ill]
  comment = Pour ILL
  path = /srv/ill
  browseable = yes
  read only = no
  guest ok = yes
~~~~

Puis on relance les services concernées et on teste en local :

~~~~Bash
$ sudo systemctl restart nmbd
$ sudo systemctl restart smbd
$ smbclient -L localhost -N
Anonymous login successful

	Sharename       Type      Comment
	---------       ----      -------
	print$          Disk      Printer Drivers
	atp            Disk      Des données...
	ill            Disk      Pour ILL
	IPC$            IPC       IPC Service (Samba 4.13.13-Debian)
SMB1 disabled -- no workgroup available
$ smbclient -L localhost -U joe
Enter WORKGROUP\joe's password: 

	Sharename       Type      Comment
	---------       ----      -------
	print$          Disk      Printer Drivers
	atp            Disk      Des données...
	ill            Disk      Pour ILL
	IPC$            IPC       IPC Service (Samba 4.13.13-Debian)
	joe             Disk      Home Directories
SMB1 disabled -- no workgroup available
$ smbclient //localhost/jpierre 
Enter WORKGROUP\jpierre's password: 
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Fri Jun  9 13:25:43 2023
  ..                                  D        0  Tue Jun  6 10:31:08 2023
  .var                               DH        0  Thu Jun  8 14:40:12 2023
...

smb: \> exit
$
~~~~

On devrait pouvoir accéder à ces partages à partir d'un poste MS Windows :
`\\morlaix\atp`, `\\morlaix\jpierre`, etc...

Note 1 : j'ai ajouté deux lignes dans `C:\Windows\System32\drivers\etc\hosts`
avec les noms et les IPs des deux systèmes GNU/Linux. (je ne peux pas administrer
le DNS de ATP...)

Note 2 : La configuration des postes ici empêche de tester l'exercice :

~~~~
C:\Users\Administrateur>net use y: \\morlaix\atp
L’erreur système 1272 s’est produite.

Vous ne pouvez pas accéder à ce dossier partagé, car les stratégies de sécurité de votre entreprise bloquent l'accès invité non authentifié. Ces stratégies contribuent à la protection de votre PC contre les périphériques non sécurisés ou malveillants du réseau.
~~~~

Il faut trouver l'endroit improbable dans la configuration de Microsoft Windows où
c'est positionné et modifier ce paramètre.

cf. https://learn.microsoft.com/fr-fr/troubleshoot/windows-client/networking/cannot-access-shared-folder-file-explorer

Méthode 2 : Activer les ouvertures de session invité non sécurisées avec l’Éditeur de stratégie de groupe locale

- Retour à l’accueil.
- Accédez à la recherche, tapez gpedit.msc, puis appuyez sur Entrée.
- Accédez à Configuration ordinateur->Modèles d’administration->Réseau->Station de travail LANMAN.
- Dans le volet droit, double-cliquez sur Activer les ouvertures de session invité non sécurisées.
- Sélectionnez Activé, puis appuyez sur Entrée.

(ça a l'air de marcher)

