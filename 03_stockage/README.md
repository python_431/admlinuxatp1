# Gestion du stockage et systèmes de fichiers

Disque locaux (SATA, SCSI) sont visibles comme périphériques dans
/dev (type block, ex : /dev/sda, /dev/sdb) ainsi que leurs partitions
(/dev/sda1, /dev/sda2).

Les tables des partitions, sur PC, on connu deux grand formats :

"MBR" ou "MSDOS" : 4 partitions "primaires" ou de 1 à 3 partions
primaires et une "étendue" (/dev/sda1, 2, 3, 4) contenant des
partition "logiques" (/dev/sda5)

GPT : GUID Partition Table, apparu en même temps qu'UEFI. 

Quant on va initialiser (créer la table des partitions, ou label)
un nouveau disque on va privilégier GPT.

But de cet atelier : déplacer /var dans une partition unique d'un
nouveau disque dur (ensuite /srv/atp dans un volume logique réparti
sur deux voire trois - ou plus - disques)

1. Ajoutez dans virtual box un disque connecté au contrôleur SATA
 (nécessite l'arrêt de la VM) 
2. avec lsblk déterminez le nom de ce nouveau périphérique
 (le seul qui n'a pas de partition, n'est monté nulle part)

Comment initialiser une table de partitions ?

(si il y a déjà une table de partition : `dd if=/dev/zero of=/dev/sdb bs=1M count=10`)

`fdisk / parted / gparted (GUI) / sfdisk (scripts) / cfdisk (mon favori)`

~~~~
$ sudo cfdisk /dev/sdb
~~~~

type : GPT
les valeurs par défaut sont OK (tout le disque, type Linux FS)

Écrire puis quitter...

~~~~
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   20G  0 disk 
├─sda1   8:1    0  512M  0 part /boot/efi
├─sda2   8:2    0 18,5G  0 part /
└─sda3   8:3    0  976M  0 part [SWAP]
sdb      8:16   0   20G  0 disk 
└─sdb1   8:17   0   20G  0 part 
sr0     11:0    1   51M  0 rom  
~~~~

On va la formatter, dans quel filesystem ?

## Les systèmes de fichies supportés par Linux : état de l'art

Historiquement :

Minix quelques mois (en 1991/92)
ext1 : pas vécu longtemps (rien à voir avec les suivants)
ext2 : la norme pendant des années (écrit par Rémy Card)
ext3 : introduit la journalisation
ext4 : sauter qq limites de tailles

* ext4 : défaut dans Debian, Ubuntu

xfs : Origine IRIX (SGI)
      Bonnes perfs, fonctionnalité temps réel

* xfs : défaut dans Red Hat et consors

= btrfs : "Butter FS", l'arlésienne, développé essentiellement
 par Oracle. Encore expérimental mais ça arrive :

https://btrfs.readthedocs.io/en/latest/Status.html

-> multi volume (plus besoin de LVM !)

= ZFS : SUN Solaris, porté sous BSD, licence dont la compatibilité
 avec la GPL est problématique

 * très fiable, très souple (multi-volumes et sous-volumes)
 * utilise 50% RAM 

-> envisageable pour un serveur NAS spécialisé (NFS)


~~~~
$ sudo apt install xfsprogs
$ sudo mkfs.<tab><tab>
mkfs.bfs     mkfs.exfat   mkfs.ext3    mkfs.fat     mkfs.msdos   mkfs.vfat    
mkfs.cramfs  mkfs.ext2    mkfs.ext4    mkfs.minix   mkfs.ntfs    mkfs.xfs 
$ sudo mkfs.xfs /dev/sdb1 
$ sudo mount /dev/sdb1 /mnt # montage provisoire seulement !!!
$ lsblk
$ lsblk --fs
$ sudo umount /mnt # bien penser à démonter... 
~~~~

Comment rendre ce montage permanent ?

- Dans fstab (ce qu'on va faire)

- Dans une unité de type "mount" de systemd (l'avenir ? peut être...)

Sauf que on envisage de monter ce filesystem dans /var

* Un puriste passerait en mode de maintenance (single user) et ferait
 tout en mode console (systemctl single) mais ça passera quand même
 en mode multi-utilisateur parce qu'il ne passe pas grand chose sur
 nos systèmes

* on veut préserver le contenu actuel de /var !!!

Ajouter dans fstab une ligne, vers la fin (avant sr0 - cdrom) :
~~~~
UUID=...  /var  xfs  defaults  0  2
~~~~

Dans l'ordre :

- Quoi ? UUID=... (le mieux) LABEL="/var" (mais on n'a pas positionné de label) /dev/sdb1 (pas stable)

On va mettre l'UUID qu'on peut peut voir avec "lsblk --fs"

- Où : chemin absolu vers un répertoire qui existe

- type de filesystem

- options de montage

- drapeau : à sauvegarder avec l'outil dump (que presque plus personne d'utilise)

- ordre du "file system check" (chkdsk) au boot : 1 pour la racine, 2 pour les autres 

~~~~
$ sudo mv /var /var.old
$ sudo mkdir /var
$ sudo mount -a # prend en compte fstab
(message ici ? problème !!!)
$ lsblk --fs
(/dev/sdb1 est-il bien monté dans /var ? si oui :)
$ sudo mv /var.old/* /var/
$ df -h
$ reboot
$ sudo rmdir /var.old
~~~~

## Logical Volume Manager

Couche intermédiaire entre disques/partitions et les systèmes de fichiers. Permet
d'agréger des disque ou des partitions pour former des volumes de stockage (similaires
à des disques mais répartis sur plusieurs disques : concaténation, stripping, RAID.

Ces volumes peuvent changer de taille à chaud (en cas d'augmentation en particulier),
on peut prendre des snapshots (instantanés) pour faire des sauvegardes.

Terminologie :

Volume physiques (PV) : disques ou partitions (type Linux LVM)

Groupes de volumes : agrégation de volumes physiques

Volumes logiques : contenus dans un groupe de volumes, destiné à être formaté (xfs, ext4, etc.) et montés

Commandes :

Volume physique  : pv* (create, etc.)

Groupe de volumes : vg* (create, extend, etc)

Volumes logiques : lv* (create, extend, etc.)

Scénario : 

- ajouter deux disques à notre contrôleur SATA
- sur chacun créer une partition unique de type Linux LVM (cfdisk)
- les initialiser comme volumes physiques
- créer un groupe de volumes (vg1) les contenant tout les deux
- y créer deux volumes logiques : destinés à /srv/atp et /srv/ill (à créer)

~~~~
$ lsblk
...
sdc      8:32   0   20G  0 disk 
sdd      8:48   0   20G  0 disk 
$ sudo cfdisk /dev/sdc
$ sudo cfdisk /dev/sdd
$ lsblk
...
sdc      8:32   0   20G  0 disk 
└─sdc1   8:33   0   20G  0 part 
sdd      8:48   0   20G  0 disk 
└─sdd1   8:49   0   20G  0 part 

$ sudo apt install lvm2
$ sudo pvcreate /dev/sdc1
$ sudo pvcreate /dev/sdd1
$ pvdisplay
$ pvscan

$ sudo vgcreate vg1 /dev/sdc1 /dev/sdd1
  Volume group "vg1" successfully created
$ sudo vgscan
$ sudo vgdisplay
$ sudo pvscan
$ sudo pvdisplay

$ sudo lvcreate --name atp -L 10G vg1
  Logical volume "atp" created.
$ sudo lvcreate --name ill -l 100%FREE vg1
  Logical volume "ill" created.
$ sudo lvscan
$ sudo lvdisplay
$ lsblk
sdc           8:32   0   20G  0 disk 
└─sdc1        8:33   0   20G  0 part 
  ├─vg1-atp 254:0    0   10G  0 lvm  
  └─vg1-ill 254:1    0   30G  0 lvm  
sdd           8:48   0   20G  0 disk 
└─sdd1        8:49   0   20G  0 part 
  └─vg1-ill 254:1    0   30G  0 lvm  
$ ls -l /dev/vg1/ 
total 0
lrwxrwxrwx 1 root root 7 13 oct.  09:54 atp -> ../dm-0
lrwxrwxrwx 1 root root 7 13 oct.  09:55 ill -> ../dm-1

$ sudo mkfs.ext4 /dev/vg1/atp
$ sudo mkfs.ext4 /dev/vg1/ill

$ sudo mkdir /srv/ill
~~~~

Dans fstab :

~~~~
/dev/vg1/atp  /srv/atp ext4 defaults 0 2
/dev/vg1/ill  /srv/ill ext4 defaults 0 2
~~~~

On déplace le contenu de /srv/atp avant de lancer les montages :

~~~~
$ sudo mv /srv/atp /srv/atp.old
$ sudo mkdir /srv/atp
$ sudo mount -a
$ lsblk
...
dc           8:32   0   20G  0 disk 
└─sdc1        8:33   0   20G  0 part 
  ├─vg1-atp 254:0    0   10G  0 lvm  /srv/atp
  └─vg1-ill 254:1    0   30G  0 lvm  /srv/ill
sdd           8:48   0   20G  0 disk 
└─sdd1        8:49   0   20G  0 part 
  └─vg1-ill 254:1    0   30G  0 lvm  /srv/ill

$ df -h

$ sudo /srv/atp.old/* /srv/atp/
$ sudo rmdir /srv/atp.old
$ df -h
~~~~

## Étendre un ou plusieurs volumes logiques

On commence par ajouter encore un disque de 20Gio et on y créer une
partition de type Linux LVM, qu'on initialise et ajoute au groupe
de volumes vg1 :

~~~~
$ lsblk
...
sde           8:64   0   20G  0 disk 
$ sudo cfdisk /dev/sde
$ lsblk
...
sde           8:64   0   20G  0 disk 
└─sde1        8:65   0   20G  0 part 

$ sudo pvcreate /dev/sde1
$ sudo vgextend vg1 /dev/sde1
  Volume group "vg1" successfully extended
~~~~

On peut alors augmenter la taille d'un ou plusieurs des volumes
logique qu'il contient :

~~~~
$ df -h /srv/atp
Sys. de fichiers    Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/vg1-atp   9,8G     24K  9,3G   1% /srv/atp

$ sudo lvextend -L +5G /dev/vg1/atp
  Size of logical volume vg1/atp changed from 10,00 GiB (2560 extents) to 15,00 GiB (3840 extents).
  Logical volume vg1/atp successfully resized.

$ df -h /srv/atp
Sys. de fichiers    Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/vg1-atp   9,8G     24K  9,3G   1% /srv/atp
~~~~

Le système de fichier n'a pas grandi : il faut le demander avec un outil
spécifique au type de système de fichier (resize2fs ou xfs_growfs) :

~~~~
$ sudo resize2fs /dev/vg1/atp 
resize2fs 1.46.2 (28-Feb-2021)
Filesystem at /dev/vg1/atp is mounted on /srv/atp; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 2
The filesystem on /dev/vg1/atp is now 3932160 (4k) blocks long.

$ df -h /srv/atp
Sys. de fichiers    Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/vg1-atp    15G     24K   14G   1% /srv/atp
~~~~

On peut aussi demander à lvextend de le faire :

~~~~
$ sudo lvextend -l +50%FREE --resizefs /dev/vg1/ill
  Size of logical volume vg1/ill changed from 29,99 GiB (7678 extents) to 37,49 GiB (9598 extents).
  Logical volume vg1/ill successfully resized.
resize2fs 1.46.2 (28-Feb-2021)
Filesystem at /dev/mapper/vg1-ill is mounted on /srv/ill; on-line resizing required
old_desc_blocks = 4, new_desc_blocks = 5
The filesystem on /dev/mapper/vg1-ill is now 9828352 (4k) blocks long.
$ df -h
...
/dev/mapper/vg1-ill    37G     24K   35G   1% /srv/ill

$ lsblk
...
dc           8:32   0   20G  0 disk 
└─sdc1        8:33   0   20G  0 part 
  ├─vg1-atp 254:0    0   15G  0 lvm  /srv/atp
  └─vg1-ill 254:1    0 37,5G  0 lvm  /srv/ill
sdd           8:48   0   20G  0 disk 
└─sdd1        8:49   0   20G  0 part 
  └─vg1-ill 254:1    0 37,5G  0 lvm  /srv/ill
sde           8:64   0   20G  0 disk 
└─sde1        8:65   0   20G  0 part 
  ├─vg1-atp 254:0    0   15G  0 lvm  /srv/atp
  └─vg1-ill 254:1    0 37,5G  0 lvm  /srv/ill
~~~~

Il reste de l'espace libre dans le groupe de volume (volontaire : ça permet de prendre des instantanés) :

~~~~
$ sudo vgdisplay
...
  Free  PE / Size       1919 / <7,50 GiB
~~~~

## Instantanés de volumes logiques

Scénario type : on a une base de données quelconque qui écrit dans un volume
et on souhaite sauvegardé les données : on interrompt la base de données
on créé l'instantané et on relance la base de donnée. On monte l'instantané
dans (mettons) /mnt, on le sauvegarde, on le démonte et on supprime l'instantané.

Le mécanisme sous-jacent est COW (Copy On Write) : l'espace disponible dans le
groupe de volume est utilisé pour stocké les différences entre l'état de l'instantané
et le volume concerné (en pratique on 10 à 20% de la taille du volume à sauvegarder
suffit, si l'instantané doit être gardé indéfiniment il faut 110% du volume logique
d'origine).

ex: pour /dev/vg1/atp

~~~~
... on arrète le service qui écrit dans le volume : systemctl stop postgresql
$ sudo lvcreate --name backup --snapshot -l 100%FREE /dev/vg1/atp
  Logical volume "backup" created.
... on relance le service : systemctl start postgresql (les écritures continues dans /srv/atp) sans
... se voir dans /dev/vg1/backup (i.e. /mnt ensuite)
$ sudo mount /dev/vg1/backup /mnt
$ df -h
$ lsblk
... on sauvegarde [le backup est gelé, il ne change pas même si pg écrit dans /srv/atp]
... par exemple :
... tar jcf /root/backup.tar.bz2 /mnt
$ sudo touch /srv/atp/new_file
$ ls /srv/atp
$ ls /mnt
$ sudo umount /mnt
$ sudo lvremove /dev/vg1/backup 
Do you really want to remove active logical volume vg1/backup? [y/n]: y
  Logical volume "backup" successfully removed
~~~~


