# Utilisateurs, groupes et comptes root

Un système Linux est multi-utilisateur.

Les comptes et les groupes locaux (comptes systèmes ou réels) sont
localement définis dans quatre fichiers :

- `/etc/passwd` : les comptes sauf l'information sur le mot de passe
- `/etc/shadow` : les mots de passe et les informations d'expiration de mot de passe
- `/etc/group`  : les groupes
- `/etc/gshadow` : les mots de passe pour les groupes (très rarement utilisés)

Il existe des annuaires permettant de centraliser les comptes : NIS, LDAP, Active Directory,
etc.

Dans ce cas là : la commande  `getent` remonte les information en fusionnant les
comptes locaux et les annuaire dans le format des fichiers plats.

Deux composants concernés par les comptes et les groupes :

- PAM _(Pluggable Authentication Modules)_ : configuré dans `/etc/pam.d`
- NSS _(Name Service Switch)_ : Association entre nom et identifiants numériques :
 configuré dans `/etc/nsswitch.conf`

On va travailler uniquement avec des comptes locaux.

Outils de création et de configuration de comptes :

- `useradd` (interactif) ou `adduser` (ligne de commande) 
- `groupadd`
- `usermod` : change la configuration d'un compte utilisateur
- `passwd`: positionner les mots de passes
- `groupdel`, `userdel`, etc.

Format des bases de comptes (en particulier des fichiers plats) :

~~~~
jpierre:x:1000:1000:Jean-Pierre,,,:/home/jpierre:/bin/bash
~~~~

Champs (séparés par ':') :

- login
- x (car le mot de passe est dans shadow)
- l'uid numérique de l'utilisateur
- le gid (l'id numérique) du groupe principal de l'utilisateur : le
 groupe par défaut lors de la création d'un fichier (les distibutions
 de GNU/Linux ont fait le choix de créer automatique un groupe par
 utilisateur qui a, par défaut, le même nom et le même numéro que lui)
- Le champs GECOS : Nom complet et le prénom et autres infos (téléphone)
 séparé par ","
- Le répertoire de connexion de l'utilisateur (peut ou pas être créer
 par les outils de création d'utilisateur (`-m` si vous utilisez `useradd`)
- Si le répertoire est créé le contenu `/etc/skel` est copié dans
  ce répertoire (ex : `.bashrc`)
- Le shell de l'utilisateur (peut être changé avec `chsh`)

Pour `/etc/group` :

~~~~
sudo:x:27:jpierre
jpierre:x:1000:
~~~~

ici on voit que l'utilisateur a sudo comme groupe secondaire (parmi d'autre)
et que un groupe existe qui porte le même nom, et l'id 1000 ; le fait que
ce soit le groupe primaire de l'utilsateur se voit dans `/etc/passwd`. 

Il est préférable de passer par les outils cités ci-dessus pour administrer
les comptes locaux et d'éviter d'éditer les fichiers directement. Cependant
si vous voulez le faire : passez par `vipw` (EDITOR=nano vipw si vous
voulez force l'usage de nano, ou EDITOR=vim vipw à l'inverse).

Note sur les uid : un uid ou un gid < 1000 est, conventionnellement, un
utilisateur ou un groupe "système" : lié à un usage ou à une application.
si ils sont >= 1000 il sont liés à de "vrais" utilisateurs (des gens).

## TP : création d'utilisateurs et de groupes

1. Créer deux utilisateurs normaux `joe` et `jane` avec `adduser` ou 
 `useradd` en créant leurs répertoires de connexion (man useradd)

2. Créer deux groupes "système" `atp` et `ill` 

3. Mettre `joe` dans le groupe atp et `jane` dans les deux groupes

4. Positionner les mots de passe (simple !) de ces utilisateurs

5. Utiser `su -` pour tester en étant l'un et l'autre que tout va 
   bien

6. Expérimenter le `setgid` sur un répertoire partagé :

- Créer un répertoire `/srv/atp`
- L'attribuer au groupe atp (chgrp)
- Positionner les droits w et s sur le groupe pour ce répertoire (chown)
- En étant joe puis jane (su) créer des fichiers dans ce répertoire, à
  quel groupes appartiennent-il ?

## Création des utilisateurs 

~~~~Bash
$ sudo useradd -m joe
[sudo] Mot de passe de jpierre : 
$ id joe
uid=1001(joe) gid=1001(joe) groupes=1001(joe)
$ ls /home/
joe  jpierre
$ adduser jane
bash: adduser : commande introuvable
$ sudo adduser jane
Ajout de l'utilisateur « jane » ...
Ajout du nouveau groupe « jane » (1002) ...
Ajout du nouvel utilisateur « jane » (1002) avec le groupe « jane » ...
Création du répertoire personnel « /home/jane »...
Copie des fichiers depuis « /etc/skel »...
Nouveau mot de passe : 
Retapez le nouveau mot de passe : 
passwd: password updated successfully
Changing the user information for jane
Enter the new value, or press ENTER for the default
	Full Name []: Jane
	Room Number []: 42
	Work Phone []: 
	Home Phone []: 
	Other []: 
Cette information est-elle correcte ? [O/n]o
$ id jane
uid=1002(jane) gid=1002(jane) groupes=1002(jane)
$ ls /home/
jane  joe  jpierre
$ sudo passwd joe
Nouveau mot de passe : 
Retapez le nouveau mot de passe : 
passwd: password updated successfully
~~~~

On peut vérifier et finaliser des détails :

~~~~Bash
$ getent passwd joe jane
joe:x:1001:1001::/home/joe:/bin/sh
jane:x:1002:1002:Jane,42,,:/home/jane:/bin/bash
$ sudo chsh -s /bin/bash joe
~~~~

## Création des groupes

~~~~Bash
$ sudo groupadd -r atp
$ sudo groupadd -r ill
$ sudo getent group atp ill
atp:x:996:
ill:x:995:
~~~~

Ajout des utilisateurs dans les groupes idoines :

~~~~Bash
$ sudo usermod -a -G atp joe
$ sudo usermod -a -G atp jane
$ sudo usermod -a -G ill jane
$ id joe
uid=1001(joe) gid=1001(joe) groupes=1001(joe),996(atp)
$ id jane
uid=1002(jane) gid=1002(jane) groupes=1002(jane),996(atp),995(ill)
~~~~

Les étapes 1 à 5 sont à faire aussi sur l'autre système (le second Debian GNU/Linux
que l'on a installé)

_Notes_ : il est intéressant, ici, de mettre à jour le fichier `/etc/hosts` avec les
noms et les IPs de nos deux systèmes.

Sudo n'est pas install sur le second système, on peut l'y mettre :

~~~~Bash
$ su -
...: rootpw
# apt install sudo
# usermod -a -G sudo jpierre
~~~~

- Notez les IPs de vos deux systèmes (`ip addr show`)
- Dans les deux fichiers `/etc/hosts` de ces systèmes ajoutez des lignes correpondantes
à ces noms et IPs, chez moi :

~~~~
$ cat /etc/hosts
127.0.0.1	localhost
192.168.1.63    morlaix
192.168.1.82    brest

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
~~~~ 

## Répertoire partagé de groupes (setgid)

~~~~Bash
$ sudo mkdir /srv/atp
[sudo] Mot de passe de jpierre : 
$ sudo chgrp atp /srv/atp
$ ls -ld /srv/atp
drwxr-xr-x 2 root atp 4096 10 oct.  14:56 /srv/atp
$ sudo chmod g+ws /srv/atp
$ ls -ld /srv/atp
drwxrwsr-x 2 root atp 4096 10 oct.  14:56 /srv/atp
$ su - joe
Mot de passe : 
joe@morlaix:~$ touch /srv/atp/joe_data
joe@morlaix:~$ ls -l /srv/atp/joe_data 
-rw-r--r-- 1 joe atp 0 10 oct.  14:56 /srv/atp/joe_data
joe@morlaix:~$ exit
déconnexion
$ su - jane
Mot de passe : 
jane@morlaix:~$ touch /srv/atp/jane_data
jane@morlaix:~$ ls -l /srv/atp/
total 0
-rw-r--r-- 1 jane atp 0 10 oct.  14:57 jane_data
-rw-r--r-- 1 joe  atp 0 10 oct.  14:56 joe_data
jane@morlaix:~$ chmod g+w /srv/atp/jane_data 
jane@morlaix:~$ ls -l /srv/atp/
total 0
-rw-rw-r-- 1 jane atp 0 10 oct.  14:57 jane_data
-rw-r--r-- 1 joe  atp 0 10 oct.  14:56 joe_data
~~~~

Note : Parfois (souvent !) on supprime des comptes et on oublie d'aller chercher
les fichiers qui appartiennet à un utilisateur (il n'y a pas que /home !)

Solution : 

~~~~
$ sudo find / -nouser -o -nogroup 2> /dev/null
~~~~ 

Ça permet de trouver les fichiers qui ont un uid de propriétaire ou un de gid
de groupe qui n'existe pas comme compte.

# Devenir root : comment ?

- Le modèle traditionnel : root est un compte disposant d'un mot de passe, les
accès sur la console sont permis, les accès à travers ssh sont permis ou pas.

- Le modèle RBAC _(Role bases access control)_ : à travers un outil comme sudo
on peut déclarer une règle permettant à certains utilisateur de lancer toute 
commande ou seulement certaines d'entre elles (avec contrôle des arguments)
en tant que root

- Le plus simple dans ce cas : les membres d'un groupe prédéfini peuvent lancer
n'importe quoi en root avec sudo (y compris `sudo -s`) : groupe sudo sous 
Debian et Ubuntu et wheel sous Red Hat (Rocky, CentOS, Fedora)

Avantage : dans les logs on voit qui a fait quoi et quand dans les logs.

On peut même envisager de verrouiller le compte root :

~~~~Bash
$ sudo passwd -l root
~~~~

-> le mot de passe de root devient invalide.

# SSH et l'authentification à clef publique

- Cryptographie symmétrique : (jusqu'au années 60/70), repose sur un
secret partagé. Pb : il faut un canal de communication sûr pour échanger
ce secret. 

- Cryptographie assymétrique ou dite à clef publique (RSA) : on génère
  une paire de clefs liées entre elle sur la base d'un aléatoire sérieux

  - l'une est publique (PEUT être publiée sans problème pour le monde
    entier

  - l'autre est privée et DOIT le rester 

- Le concept est basé sur ceci : si l'on chiffre avec l'une de ces clefs
  alors on ne peut déchiffrer qu'avec l'autre 

Considérons deux interlocuteur : Alice et Bob.

Alice peut chiffrer avec sa clef privée et envoyer le résultat à Bob.

Qui peut déchiffrer ? Tout le monde (il suffit de connaître la clef
publique). Pas très secret, mais ça garantit, sous réserve qu'Alice
ait bien gardé privée sa clef privée, que c'est elle qui a émit le
message. Il faut cependant s'assurer que la clef publique est bien
celle d'Alice...

Alice peut chiffrer avec la clef publique de Bob. Qui peut déchiffrer ?
Si Bob a bien gardé privée sa clef privée : seulement lui !

SSH : Secure SHell (qui a remplacé telnet, rlogin, rsh) utilise ces
principes.

1. Au niveau hôte : chaque serveur ssh a des clef d'hôtes générés
à l'installation : /etc/ssh/ -> pas de .pub : privées, un .pub : publique.

Quand on se connecte en ssh une première fois à un serveur donné
on voit un avertissement :

~~~~Bash
$ ssh jpierre@brest
The authenticity of host 'brest (192.168.1.82)' can't be established.
ECDSA key fingerprint is SHA256:y629AkLRIZ7+fJHfIJJJ1is+leprM4LbJbE0+MZxrDg.
Are you sure you want to continue connecting (yes/no/[fingerprint])? 
~~~~

Notre client ssh, lancé sous notre identité, rençoit en clair pour
la première fois la clef d'hôte de brest.

Est-elle authentique ? Généralement (hors contexte de défense) c'est
ok...

On dit oui :

~~~~
Warning: Permanently added 'brest,192.168.1.82' (ECDSA) to the list of known hosts.
~~~~

Cette clef d'hôte est alors ajouté dans le fichier ~/.ssh/know_hosts sur le client.

Si dans l'avenir un ssh sur le même serveur fournit une clef publique différente :

- SSH va refuser la connexion
- Il va aussi nous indiquer comment oublier cette clef (en cas de réinstallation
du serveur)

Comment fonctionne les échange entre le client ssh et le serveur sshd :

- La clef publique du serveur est utilisée pour échanger une clef symmétrique
 aléatoire régulièrement (clefs de sessions)

- La cryptographie symmétrique est beaucoup plus rapide

2. Au niveau utilisateur : comment en profiter ?

- On peut créer sa propre paire de clefs publique/privée avec l'algorithme
mathématique que l'on souhaite...

~~~~Bash
$ ssh-keygen -t rsa 
~~~~

Et on répond (pour l'instant <entrée> à toutes les questions.

~~~~
(client)$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/jpierre/.ssh/id_rsa): 
Created directory '/home/jpierre/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/jpierre/.ssh/id_rsa
Your public key has been saved in /home/jpierre/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:1F02+v6rrXcj+QcrV1aSwn95ZR5ZZjhlWlhAivLd42A jpierre@brest
The key's randomart image is:
+---[RSA 3072]----+
|             .**=|
|         . o =+=+|
|        o o.+ .=o|
|       . o .oooo+|
|        S . Eo++=|
|           . +oo*|
|              +*.|
|            .o++o|
|             =*+*|
+----[SHA256]-----+
~~~~

Le fichier `.ssh/id_rsa` est alors ma clef privée, stockée en clair
et `.ssh/id_rsa.pub` ma clef publique.

Je peux "envoyer" cette clef publique vers l'autre système, sur un
compte utilisateur sur le lequel je peux me connecter :

~~~~
(client)$ ssh-copy-id -i ~/.ssh/id_rsa.pub  user_id@(serveur)
... mot de passe demandé
(client)$ ssh user_id@(client)
... pas de mot de passe demandé
(serveur)$ ...
~~~~

-> cette opération insère la clef id_rsa.pub du client dans le fichier
`~/.ssh/authorized_keys` sur le serveur.

Si tout est ok. On peut même désactiver totalement l'authentification par
mot de passe sur le serveur (`/etc/ssh/sshd_config`) :

~~~~
PasswordAuthentication no
~~~~

Note : une autre option intéressante :

~~~~
PermitRootLogin prohibit-password
~~~~

Permet de faire un `ssh root@(server)` seule l'authentification par clef
publique sera admise.

- yes est une mauvaise idée
- no est parfois ce qui est préconisé
- prohibit-password me paraît un compromis acceptable

Question : pourquoi et dans quel contexte vous pourriez vouloir chiffrer
la clef privée sur le client ?

La question préalable à se poser pour répondre à cette question est
qui (en pratique) a la possibilité de lire ce fichier.

Si on chiffre la clef privée on doit donner la phrase de passe lors
des connexions. Ce qui supprime la possibilité de se connecter sans
mot de passe... Une solution `ssh-agent` et `ssh-add` : on aura la
phrase de passe qu'une seule fois par session sur le client.

En résumé : la sécurité de SSH repose sur :

- Le caractère privé de la clef privée sur le client

- L'authenticité de la clef publique du serveur (elle
est acceptée à la première connexion et enregistrée, 
et vérifiée lors des connextion ultérieures)

- Sur ces bases il est intéressant une fois que c'est
mis en place de désactivé l'authentification par mot
de passe sur le serveur SSH (bonne pratique) 

- Dans ce contexte permettre l'accès root à distance
sans mot de passe n'est pas nécessairement pb de
sécurité 

## Configurer l'authentification SSH par clef publique

1. On souhaite pouvoir se connecter de Debian 1 à Debian 2 sans mot de passe avec une clef ssh privée stockée en clair.

- Sur Debian 1 on va avoir (créé par ssh-keygen -t rsa)
dans ~/.ssh les fichier id_rsa (privée, en clair) et
id_rsa.pub (publique)

(pensez à vérifier dans ~/.ssh si ces fichiers sont déjà
là ou pas)

- En partant de Debian 1 on tranfère id_rsa.pub dans
le fichier ~/.ssh/authorized_keys avec : 
`ssh-copy-id -i ~/.ssh/id_rsa.pub identifiant@ip_de_debian2`

- On teste : en partant de Debian 1 vous devriez pouvoir
faire ssh user_id@10.0.0.2 et obtenir un Shell sans mot
de passe


Note sur les droits (si vous manipulez ~/.ssh à la main) :

- .ssh : 700 (rwx-------)
- .ssh/id*pub : 744 (rwxr--r--) (700 est ok)
- .ssh/id* : 600 (rw-------)
- .ssh/authorized_keys : 600 (rw------) (côté serveur)


Comme on est passé en réseau privée VBox on va rapidement
leur attribuer des IP fixes :

Nom de l'interface réseau : `ip link show` : enp0s3

Un fichier (à l'ancienne) de conf du réseau (on verra nmcli
plus tard) : `/etc/network/interfaces`

~~~~
auto enp0s3
iface enp0s3 inet static
    address 10.0.0.X/24 (1 ou 2)
~~~~

enuite :

~~~~Bash
$ sudo ifdown -a
$ sudo ifup -a
$ ip addr show
$ ping 10.0.0.Y (l'autre ip)
~~~~

2. On va créer sur le Debian 2 une paire de clef ssh mais
cette fois avec une phrase de passe qui chiffre la clef
privée (ssh-keygen -t rsa)

On copie la clef publique correspondante sur Debian 1 comme
précédemment : ssh-copy-id ~/.ssh/id_rsa.pub userid@10.0.0.1

Que ce passe-t-il quand vous testez en faisant sur Debian 2 :
ssh userid@10.0.0.1 ? La phrase de passe est demandé ?

Comment ne pas voir à saisir cette phrase à chaque connexion :

- il faut que un agent ssh tourne sur le client (c'est le
cas si vous êtes sur un bureau Linux comme Gnome ou macOS)

- on peut le lancer dans un terminal :

~~~~Bash
$ eval $(ssh-agent)
$ ssh-add ~/.ssh/id_rsa
-> la phrase de passe est demandée
$ ssh user_id@10.0.0.1 -> elle n'est plus demandé
~~~~

