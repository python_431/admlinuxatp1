https://framagit.org/jpython/meta

Installation :
Virtual Box sur le bureau : màj en 7.0.10

Télécharger l'image ISO de la dernière Debian
stable dite "netinst" (iso minimal d'installation)

https://www.debian.org/CD/netinst/

Image de CD d'installatin par le réseau / amd64
https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.2.0-amd64-netinst.iso

Un souci non-déterminé avec Virtual Box et Debian 12
on va installer une Debian 11 (et mettre à jour en 12 ensuite)


https://cdimage.debian.org/cdimage/archive/latest-oldstable/amd64/iso-cd/
télécharger : 	debian-11.8.0-amd64-netinst.iso

Créer dans Virtual Box une nouvelle VM
- 2 coeurs
- 4 Gio RAM
- Disque de 20 Gio (non préalloué)
- skip unattended installation (par défaut virtual box passe
outre l'installeur) -> on veut pas de ça
- Enable EFI
- Réseau : accès par pont
- Démarrer puis "Install" (installation en mode texte)

-> nom du sytème : nom de ville en minuscule
-> domaine dns : vide
-> mot de passe root : rootpw 
-> Utilisateur : votre prénom + login simple
(Jean-Pierre et jpierre)
-> mdp : userpw
-> assisté / utiliser un disque entier
-> Tout sur une seule partition
-> ajouter Serveur SSH et lancer l'installation : PAUSE

-> finaliser l'installation : installer "ajouts invités"
(pilotes spécifique à Virtual Box qui permettent :
 - le bureau occupe tout l'écran
 - le presse papier partagé avec MS Windows

~~~~ 
 $ su -
 (mdp : rootpw) 
 # usermod -a -G sudo jpierre (changer avec login)
 # apt install build-essential dkms
 -> Menu Périphérique / Insérer l'image CD des additions invités
 # mount /media/cdrom
 # bash /media/cdrom/VBoxLinuxAdditions.run
 ...
 # reboot
 -> Menu Périphérique  | Presse papier partagé | Bidirectionnel
 -> tester le copier collé (ça marche !)
~~~~
 
 Créer une seconde VM Virtual Box avec la même conf matériel
 en déselectionnant bureau graphique / Gnome à la fin
 
# À vous : installez git et clonez ce dépôt :

~~~~
$ sudo apt install git
$ git clone https://gitlab.com/python_431/admlinuxatp1.git
$ cd admlinuxatp1
$ ls
~~~~

